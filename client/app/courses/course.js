angular.module('app')
  .config(['$stateProvider', function ($stateProvider) {
    'use strict';
 
    $stateProvider.state('courses', { // this is a name for our route
      url: '/courses', // the actual url path of the route
      templateUrl: 'app/courses/course.html', // the template that will load
      controller: 'CourseCtrl' // the name of the controller to use
    });
  }]);