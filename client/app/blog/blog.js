angular.module('app')
  .config(['$stateProvider', function ($stateProvider) {
    'use strict';

    $stateProvider.state('blog', { // this is a name for our route
      url: '/blog', // the actual url path of the route
      templateUrl: 'app/blog/blog.html', // the template that will load
      controller: 'BlogCtrl' // the name of the controller to use
    })
    .state('blogDetails', {
          url: '/blog/:id', // the actual url path of the route
          templateUrl: 'app/blog/blogarticle.html' // the template that will load
          //controller: 'BlogArticleCtrl' // the name of the controller to use
        });
  }]);